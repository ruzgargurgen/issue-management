package com.ruzgargurgen.issuemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ruzgargurgen.issuemanagement.entity.Issue;

public interface IssueRepository extends JpaRepository<Issue, Long> {

}
